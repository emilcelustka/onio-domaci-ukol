let input = document.getElementById('input');

function increaseIt() {
    let inputVal = Number(document.getElementById('input').value);
    let newVal = inputVal + 1;
    input.setAttribute('value', newVal);
}

function decreaseIt() {
    let inputVal = Number(document.getElementById('input').value);
    if(inputVal != 1) {
        let newVal = inputVal - 1;
        input.setAttribute('value', newVal);
    }
}
document.getElementById('increaseBtn').addEventListener('click', increaseIt);
document.getElementById('decreaseBtn').addEventListener('click', decreaseIt);